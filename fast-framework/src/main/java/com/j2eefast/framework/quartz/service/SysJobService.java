/*
 * All content copyright http://www.j2eefast.com, unless 
 * otherwise indicated. All rights reserved.
 * No deletion without permission
 */
package com.j2eefast.framework.quartz.service;


import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.j2eefast.common.core.exception.RxcException;
import com.j2eefast.common.core.page.Query;
import com.j2eefast.common.core.utils.PageUtil;
import com.j2eefast.common.core.utils.ToolUtil;
import com.j2eefast.framework.quartz.entity.SysJobEntity;
import com.j2eefast.framework.quartz.mapper.SysJobMapper;
import com.j2eefast.framework.quartz.utils.JobInvokeUtil;
import com.j2eefast.framework.quartz.utils.ScheduleUtils;
import com.j2eefast.framework.utils.Constant;
import com.j2eefast.framework.utils.CronUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * 定时任务服务实现类
 * @author zhouzhou
 * @date 2020-03-08 16:07
 */
@Service
@Slf4j
public class SysJobService extends ServiceImpl<SysJobMapper,SysJobEntity>{

	@Autowired
	private Scheduler scheduler;

	@Resource
	private SysJobMapper sysJobMapper;

	/**
	 * 页面展示查询翻页
	 */
	public PageUtil findPage(Map<String, Object> params) {
		String jobName = (String) params.get("jobName");
		String jobGroup = (String) params.get("jobGroup");
		String status = (String) params.get("status");
		Page<SysJobEntity> page = this.baseMapper.selectPage(new Query<SysJobEntity>(params).getPage(),
				new QueryWrapper<SysJobEntity>().like(ToolUtil.isNotEmpty(jobName), "job_name", jobName)
						.like(ToolUtil.isNotEmpty(jobGroup), "job_group", jobGroup)
						.eq(ToolUtil.isNotEmpty(status), "status", status)
		);
		page.getRecords().forEach(r->{
			r.setNextDate(CronUtils.getNextExecution(r.getCronExpression()));
		});
		return new PageUtil(page);
	}
	
	
	/**
	 * 新增任务
	 * @author zhouzhou
	 * @date 2020-03-08 16:33
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean add(SysJobEntity sysJob) {
		if(this.save(sysJob)){
			ScheduleUtils.createScheduleJob(scheduler, sysJob);
			return true;
		}
		return false;
	}


	/**
	 * 新增业务关联定时任务
	 * @param sysJob
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean addBusJob(SysJobEntity sysJob) {
		Assert.isNull(sysJob.getCronExpression(),"cron表达式不能为空");
		if(!CronExpression.isValidExpression(sysJob.getCronExpression())){
			throw new RxcException("cron表达式不正确","20001");
		}
		Assert.isNull(sysJob.getInvokeTarget(),"调用目标字符串不能为空");
		Assert.isNull(sysJob.getBizId(),"业务ID不能为空");
		if(ToolUtil.isEmpty(sysJob.getJobName())){
			sysJob.setJobName("BUS-JOB");
		}
		if(ToolUtil.isEmpty(sysJob.getJobGroup())){
			sysJob.setJobGroup("BUS");
		}
		if(ToolUtil.isEmpty(sysJob.getMisfirePolicy())){
			sysJob.setMisfirePolicy("2");
		}
		if(ToolUtil.isEmpty(sysJob.getConcurrent())){
			sysJob.setConcurrent("1");
		}
		if(this.save(sysJob)){
			ScheduleUtils.createScheduleJob(scheduler, sysJob);
			return true;
		}
		return false;
	}


	/**
	 * 更新定时任务
	 * @param job
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public boolean updateSysJob(SysJobEntity job) throws Exception {
		SysJobEntity properties = this.getById(job.getId());
		if (this.updateById(job)){
			updateSchedulerJob(job, properties.getJobGroup());
			return true;
		}
		return false;
	}


	/***
	 *  更新任务
     * @param job 任务对象
     * @param jobGroup 任务组名
     */
	public void updateSchedulerJob(SysJobEntity job, String jobGroup) throws Exception{

		Long jobId = job.getId();
		// 判断是否存在
		JobKey jobKey = ScheduleUtils.getJobKey(jobId, jobGroup);

		if (scheduler.checkExists(jobKey)){
			// 防止创建时存在数据问题 先移除，然后在执行创建操作
			scheduler.deleteJob(jobKey);
		}

		ScheduleUtils.createScheduleJob(scheduler, job);
	}

	/**
	 * 检查定时任务是否存在
	 * @param id
	 * @return
	 */
	public boolean isCheckExistsJob(Long id){
		SysJobEntity job = this.getById(id);
		if(!ScheduleUtils.checkExists(scheduler,job)){
			log.debug("---------- 定时任务不存在:{}",
					job.getJobName() + "("+job.getId()+")");
			return false;
		}else {
			log.debug("---------- 定时任务存在:{}",
					job.getJobName() + "("+job.getId()+")");
			return true;
		}
	}

	/**
	 * 强制重建定时任务
	 * @param id
	 * @throws SchedulerException
	 */
	public void foredCreation(Long id) throws SchedulerException {
		SysJobEntity job = this.getById(id);
		if(ScheduleUtils.checkExists(scheduler,job)){
			scheduler.deleteJob(ScheduleUtils.getJobKey(job.getId(),job.getJobGroup()));
		}
		ScheduleUtils.createScheduleJob(scheduler,job);
	}

	/**
	 * 立即运行任务
	 *
	 * @param jobIds 任务ids
	 */
	@Transactional
	public void run(Long[] jobIds) throws SchedulerException{
		for(Long id : jobIds){
			SysJobEntity tmpObj = this.getById(id);
			// 参数
			JobDataMap dataMap = new JobDataMap();
			dataMap.put(JobInvokeUtil.TASK_PROPERTIES, tmpObj);
			scheduler.triggerJob(ScheduleUtils.getJobKey(id, tmpObj.getJobGroup()), dataMap);
		}
	}

	/**
	 * 根据业务ID 立即运行
	 * @param bizId 业务ID
	 */
	public void runByBizId(Long bizId){

		List<SysJobEntity> jobEntityList = this.list(new QueryWrapper<SysJobEntity>()
				.eq("biz_id",bizId));
		if(ToolUtil.isEmpty(jobEntityList)){
			return;
		}

		jobEntityList.forEach(e->{
			// 参数
			JobDataMap dataMap = new JobDataMap();
			dataMap.put(JobInvokeUtil.TASK_PROPERTIES, e);
			try {
				scheduler.triggerJob(ScheduleUtils.getJobKey(e.getId(), e.getJobGroup()), dataMap);
			} catch (SchedulerException schedulerException) {
			}
		});

	}


	/**
	 * 删除任务后，所对应的trigger也将被删除
	 * @param job 调度信息
	 */
	@Transactional
	public boolean deleteJob(SysJobEntity job) throws SchedulerException {
		Long jobId = job.getId();
		String jobGroup = job.getJobGroup();
		if (this.removeById(jobId)){
			scheduler.deleteJob(ScheduleUtils.getJobKey(jobId, jobGroup));
			return true;
		}
		return false;
	}


	/**
	 * 根据主键删除定时任务
	 * @param id
	 * @return
	 * @throws SchedulerException
	 */
	public boolean removeById(Long id)  throws SchedulerException {
		SysJobEntity job = this.getById(id);
		if(this.removeById(id)){
			scheduler.deleteJob(ScheduleUtils.getJobKey(job.getId(), job.getJobGroup()));
			return true;
		}
		return false;
	}

	/**
	 * 根据业务ID 删除任务
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean removeByBiz(Long id) {

		List<SysJobEntity> jobEntityList = this.list(new QueryWrapper<SysJobEntity>()
				.eq("biz_id",id));

		if(ToolUtil.isEmpty(jobEntityList)){
			return true;
		}

		jobEntityList.forEach(e->{
			if(this.sysJobMapper.deleteById(e.getId()) > 0){
				try {
					scheduler.deleteJob(ScheduleUtils.getJobKey(e.getId(), e.getJobGroup()));
				} catch (SchedulerException schedulerException) {
				}
			}
		});

		return true;
	}
	
	@Transactional
	public void deleteBatchByIds(Long[] jobIds) throws SchedulerException {
		
		for (Long jobId : jobIds) {
			SysJobEntity job = this.getById(jobId);
			deleteJob(job);
		}
	}
	
	
	/**
	 * 批量更新状态
	 * @author zhouzhou
	 * @date 2020-03-08 16:58
	 */
	public int updateBatchStatus(Long[] jobIds, String status) {
		
		return this.baseMapper.updateBatchStatus(status,jobIds);
		
	}


	/**
	 * 修改状态
	 * @param job
	 * @return
	 * @throws SchedulerException
	 */
	public boolean changeStatus(SysJobEntity job) throws SchedulerException{
		String status = job.getStatus();
		if (Constant.ScheduleStatus.NORMAL.getValue().equals(status)) {
			return resumeJob(job);
		}
		else if (Constant.ScheduleStatus.PAUSE.getValue().equals(status)) {
			return pauseJob(job);
		}
		return false;
	}

	/**
	 * 恢复任务
	 *
	 * @param job 调度信息
	 */
	@Transactional
	public boolean resumeJob(SysJobEntity job) throws SchedulerException {
		Long jobId = job.getId();
		String jobGroup = job.getJobGroup();
		job.setStatus(Constant.ScheduleStatus.NORMAL.getValue());
		if (this.updateById(job)){
			scheduler.resumeJob(ScheduleUtils.getJobKey(jobId, jobGroup));
			return true;
		}
		return false;
	}

	/**
	 * 根据业务ID 恢复任务
	 * @param bizId
	 * @return
	 * @throws SchedulerException
	 */
	public boolean resumeJobByBizId(Long bizId) throws SchedulerException {
		List<SysJobEntity> jobEntityList = this.list(new QueryWrapper<SysJobEntity>()
				.eq("biz_id",bizId));
		if(ToolUtil.isEmpty(jobEntityList)){
			return true;
		}
		jobEntityList.forEach(e->{
			try {
				resumeJob(e);
			} catch (SchedulerException schedulerException) {
			}
		});
		return true;
	}

	/**
	 * 暂停服务
	 * @param job
	 * @return
	 * @throws SchedulerException
	 */
	@Transactional
	public boolean pauseJob(SysJobEntity job) throws SchedulerException {
		Long jobId = job.getId();
		String jobGroup = job.getJobGroup();
		job.setStatus(Constant.ScheduleStatus.PAUSE.getValue());
		if (this.updateById(job)){
			scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
			return true;
		}
		return false;
	}

	/**
	 * 根据业务ID 暂停业务
	 * @param bizId
	 * @return
	 * @throws SchedulerException
	 */
	public boolean pauseJobByBizId(Long bizId) throws SchedulerException {
		List<SysJobEntity> jobEntityList = this.list(new QueryWrapper<SysJobEntity>()
				.eq("biz_id",bizId));
		if(ToolUtil.isEmpty(jobEntityList)){
			return true;
		}
		jobEntityList.forEach(e->{
			try {
				pauseJob(e);
			} catch (SchedulerException schedulerException) {
			}
		});
		return true;
	}

	
	
	/**
	 * 验证表达式是否有效
	 * @author zhouzhou
	 * @date 2020-03-08 17:01
	 */
	public boolean checkCronExpressionIsValid(String cronExpression) {
		return CronUtils.isValid(cronExpression);
	}

	
}
