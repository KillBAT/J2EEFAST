/*
 * All content copyright http://www.j2eefast.com, unless
 * otherwise indicated. All rights reserved.
 * No deletion without permission
 */
package com.j2eefast.framework.quartz.utils;

import com.j2eefast.common.core.utils.RedisUtil;
import com.j2eefast.framework.quartz.entity.SysJobEntity;
import org.quartz.CronExpression;

import java.util.Date;

/**
 * 抽象业务定时
 * @author huanzhou
 */
public abstract class AbstratBusJob {

    /**
     * 缓存操作
     */
    protected RedisUtil redisUtil;

    /**
     * 执行定时任务
     * @param sysJob
     */
    protected abstract void  execute(SysJobEntity sysJob);


    public void initData(SysJobEntity sysJob){
        this.redisUtil = RedisUtil.me();
        execute(sysJob);
    }

    public boolean isOnlyOnce(String cronExpression){
        try{
            CronExpression cron = new CronExpression(cronExpression);
            Date nextValidTmie = cron.getNextValidTimeAfter(new Date());
            Date seconValidTime = cron.getNextValidTimeAfter(nextValidTmie);
            return seconValidTime == null;
        }catch (Exception e){
            return false;
        }
    }
}
