/*
 * All content copyright http://www.j2eefast.com, unless
 * otherwise indicated. All rights reserved.
 * No deletion without permission
 */
package com.j2eefast.common.event;

import com.j2eefast.common.core.enums.OperationEventType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author huanzhou
 * @date 2023/10/10
 */
@Getter
@Setter
public class SysConfigEvent extends ApplicationEvent {

    private String paramKey;
    private String appNo;
    private OperationEventType operationEventType;

    public SysConfigEvent(Object source) {
        super(source);
    }

    public SysConfigEvent() {
        super(new Object());
    }
}
