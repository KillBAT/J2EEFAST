/*
 * All content copyright http://www.j2eefast.com, unless
 * otherwise indicated. All rights reserved.
 * No deletion without permission
 */
package com.j2eefast.common.core.enums;

/**
 * @author huanzhou
 * @date 2023/10/10
 */
public enum OperationEventType {
    DEL, // 删除
    UPDATE, // 更新
    ADD, // 新增
    CLEAR; // 清理
}
